<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Catalogue netflix</title>
    <script type="text/javascript" src="apiService.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico">
  </head>
  <body onLoad="getData()">
    <img src="images/netflixLogo.png" alt="Netflix" width="500>
    <div class="table-responsive-sm">
      <table class="table table-striped table-hover" style="max-width: 900px;"></table>
    </div>
  </body>
</html>