function getData() {
    fetch("catalog").then(function(response) {
        return response.json();
    }).then(function(data) {
        let table = document.querySelector("table");
        generateTable(table, data);
        generateTableHead(table);
    }).catch(function() {
        console.log("Warning : Netflix Catalog error");
    });
}

function generateTableHead(table) {
    let customRows = ["Nom", "Saisons", "Total épisodes"];
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of customRows) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
    }
}

function generateTable(table, data) {
    for (let element of data) {
        let row = table.insertRow();
        for (key in element) {
            let cell = row.insertCell();
            let text = "";
            if(element.hasOwnProperty(key))
                text = document.createTextNode(element[key]);
            cell.appendChild(text);
        }
    }
}