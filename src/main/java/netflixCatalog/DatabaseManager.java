package netflixCatalog;

import netflixCatalog.DO.SeasonDO;
import netflixCatalog.DO.SerieDO;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {
    public DatabaseManager(){}

    public List<SerieDO> getSeries(){
        List<SerieDO> series = new ArrayList();
        series.add(new SerieDO(1, "La casa de papel", true, 2017));
        series.add(new SerieDO(2, "The 100", false, 2017));
        series.add(new SerieDO(3, "Black Mirror", true, 2017));
        return  series;
    }

    public List<SeasonDO> getSeasons(){
        List<SeasonDO> series = new ArrayList();
        series.add(new SeasonDO(1, 1, 1, 13));
        series.add(new SeasonDO(2, 1, 2, 9));
        series.add(new SeasonDO(3, 2, 1, 13));
        series.add(new SeasonDO(4, 2, 2, 16));
        series.add(new SeasonDO(5, 2, 3, 16));
        series.add(new SeasonDO(6, 2, 4, 13));
        series.add(new SeasonDO(7, 3, 1, 3));
        series.add(new SeasonDO(8, 3, 2, 4));
        series.add(new SeasonDO(9, 3, 3, 6));
        series.add(new SeasonDO(10, 3, 4, 6));
        return  series;
    }
}
