package netflixCatalog.DAO;

import netflixCatalog.DatabaseManager;
import netflixCatalog.DO.SerieDO;

import java.util.List;

public class SerieDAO {
    private DatabaseManager databaseManager;
    public SerieDAO(){
        databaseManager = new DatabaseManager();
    }

    public List<SerieDO> selectAll(){
        return databaseManager.getSeries();
    }
}
