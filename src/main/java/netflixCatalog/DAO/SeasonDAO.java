package netflixCatalog.DAO;

import netflixCatalog.DatabaseManager;
import netflixCatalog.DO.SeasonDO;

import java.util.List;

public class SeasonDAO {
    private DatabaseManager databaseManager;
    public SeasonDAO(){
        databaseManager = new DatabaseManager();
    }

    public List<SeasonDO> selectAll(){
        return databaseManager.getSeasons();
    }
}
