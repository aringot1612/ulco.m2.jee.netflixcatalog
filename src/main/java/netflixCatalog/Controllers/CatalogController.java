package netflixCatalog.Controllers;

import com.google.gson.Gson;
import netflixCatalog.BO.CatalogBO;
import netflixCatalog.DTO.CatalogDTO;

import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/catalog")
public class CatalogController extends HttpServlet {
    private CatalogBO catalogBO;
    private static final long serialVersionUID = 1L;

    public CatalogController(){
        this.catalogBO = new CatalogBO();
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final List<CatalogDTO> dto = this.catalogBO.retrieveCatalog();
        response.setContentType("application/json");
        response.getWriter().append(new Gson().toJson(dto));
    }


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}