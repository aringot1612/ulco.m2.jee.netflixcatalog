package netflixCatalog.DO;

public class SeasonDO {
    private int id;
    private int fkSerie;
    private int numSeasons;
    private int nbEpisodes;

    public SeasonDO(int id, int fkSerie, int numSeasons, int nbEpisodes){
        this.id = id;
        this.fkSerie = fkSerie;
        this.numSeasons = numSeasons;
        this.nbEpisodes = nbEpisodes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFkSerie() {
        return fkSerie;
    }

    public void setFkSerie(int fkSerie) {
        this.fkSerie = fkSerie;
    }

    public int getNumSeasons() {
        return numSeasons;
    }

    public void setNumSeasons(int numSeasons) {
        this.numSeasons = numSeasons;
    }

    public int getNbEpisodes() {
        return nbEpisodes;
    }

    public void setNbEpisodes(int nbEpisodes) {
        this.nbEpisodes = nbEpisodes;
    }
}
