package netflixCatalog.DO;

public class SerieDO {
    private int id;
    private String name;
    private boolean originalProduction;
    private int year;

    public SerieDO(int id, String name, boolean originalProduction, int yeay){
        this.id = id;
        this.name = name;
        this.originalProduction = originalProduction;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getOriginalProduction() {
        return originalProduction;
    }

    public void setOriginalProduction(boolean originalProduction) {
        this.originalProduction = originalProduction;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
