package netflixCatalog.DTO;

public class CatalogDTO {
    private String name;
    private int nbSeasons;
    private int totalEp;

    public CatalogDTO(String name, int nbSeasons, int totalEp){
        this.name = name;
        this.nbSeasons = nbSeasons;
        this.totalEp = totalEp;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNbSeasons() {
        return nbSeasons;
    }

    public void setNbSeasons(int nbSeasons) {
        this.nbSeasons = nbSeasons;
    }

    public int getTotalEp() {
        return totalEp;
    }

    public void setTotalEp(int totalEp) {
        this.totalEp = totalEp;
    }
}
