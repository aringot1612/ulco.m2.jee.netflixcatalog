package netflixCatalog.BO;

import netflixCatalog.DAO.SeasonDAO;
import netflixCatalog.DAO.SerieDAO;
import netflixCatalog.DO.SeasonDO;
import netflixCatalog.DO.SerieDO;
import netflixCatalog.DTO.CatalogDTO;

import java.util.ArrayList;
import java.util.List;

public class CatalogBO {
    private SerieDAO serieDAO;
    private SeasonDAO seasonDAO;

    public CatalogBO(){
        this.serieDAO = new SerieDAO();
        this.seasonDAO = new SeasonDAO();
    }

    public List<CatalogDTO> retrieveCatalog(){
        List<SerieDO> series = this.serieDAO.selectAll();
        List<SeasonDO> seasons = this.seasonDAO.selectAll();
        List<CatalogDTO> catalog = new ArrayList<>();
        for(SerieDO serie : series){
            catalog.add(new CatalogDTO(serie.getName(), getnbSeasonsBySerie(serie.getId(), seasons), getnbEpBySerie(serie.getId(), seasons)));
        }
        return catalog;
    }

    private int getnbSeasonsBySerie(int id, List<SeasonDO> seasons){
        return (int) seasons.stream().filter(s -> s.getFkSerie() == id).count();
    }

    private  int getnbEpBySerie(int id, List<SeasonDO> seasons){
        int sum = 0;
        for(SeasonDO element : seasons)
            if(element.getFkSerie() == id)
                sum += element.getNbEpisodes();
        return sum;
    }
}
